class Ficha < ApplicationRecord
  belongs_to :expediente

  mount_uploader :archivo, ArchivoFichaUploader

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :expediente_id, :fecha_inicio, :referencia,  
    presence: true

  validates :descripcion, length: { maximum: 500 }

  validate :fecha_anterior_a_hoy

  def fecha_anterior_a_hoy
    if fecha_inicio > Date.today
      errors.add(:fecha_inicio, "La fecha de inicio no puede ser posterior a la fecha actual.")
    end
  end
    
end
