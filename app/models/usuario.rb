class Usuario < ApplicationRecord
  has_one :consultor

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :rememberable #, :registerable,
         # :recoverable, :rememberable, :validatable

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :email, 
    uniqueness: true,
    presence: true
end
