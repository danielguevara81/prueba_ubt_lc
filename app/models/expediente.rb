class Expediente < ApplicationRecord
  has_many :fichas
  belongs_to :empresa

  mount_uploader :archivo, ArchivoExpedienteUploader

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :empresa_id, :referencia,
    presence: true
  
  validates :codigo,
    uniqueness: true,
    presence: true

end
