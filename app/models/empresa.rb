class Empresa < ApplicationRecord
  has_many :consultors
  has_many :expedientes

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :nombre, :nif, 
    presence: true

end
