class Consultor < ApplicationRecord
  belongs_to :usuario
  belongs_to :empresa

  # --------------- VALIDACIONES DE LOS CAMPOS --------------------------------------------
  validates :usuario_id, :empresa_id, :nombre, :apellidos,  
    presence: true

end