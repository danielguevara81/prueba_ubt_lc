// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

Rails.start()
Turbolinks.start()
ActiveStorage.start()

require('datatables.net-bs4')

import $ from 'jquery';
global.$ = jQuery;

import 'bootstrap'
import '@popperjs/core';

require("moment/locale/es")
require("tempusdominus-bootstrap-4")

$(document).on("turbolinks:load", function () {
  $('#myDataTable').DataTable({
    // ajax: ...,
    // autoWidth: false,
    // pagingType: 'full_numbers',
    // processing: true,
    // serverSide: true,
  
    // Optional, if you want full pagination controls.
    // Check dataTables documentation to learn more about available options.
    // http://datatables.net/reference/option/pagingType
    responsive: true,
    language: { url: "//cdn.datatables.net/plug-ins/1.10.10/i18n/Spanish.json" }
  });

  $('#ficha_fecha_inicio').datetimepicker({
    locale: 'es',
    format: 'L'
  });

});