class ExpedientesController < ApplicationController
  #---------------------------
  def index
    @titulo   = "Expedientes"
    empresa = Usuario.where("id = #{current_usuario.id}").first.consultor.empresa
    @subtitulo      = "Listado de expedientes de la empresa '#{empresa.nombre}'"
    @expedientes = empresa.expedientes;
  end

  #---------------------------
  def show
    begin
      @registro = Expediente.find(params[:id])
      @titulo   = "Detalle de expediente " + @registro.codigo
      @subtitulo = "Empresa '#{@registro.empresa.nombre}'"
      @registroFicha = Ficha.new

    rescue => e
      flash[:error] = 'Error al intentar mostrar el registro. Intente de nuevo o comuniquese con el Proveedor del servicio.'
      redirect_to empresas_path
    end
  end

  #---------------------------
  def new
    begin
      empresa = Usuario.where("id = #{current_usuario.id}").first.consultor.empresa
      @titulo   = "Expedientes"
      @subtitulo = "Crear expediente de la empresa '#{empresa.nombre}'"

      @registro = Expediente.new

      
    rescue => e
      flash[:error] = 'Error al intentar mostrar la pantalla.'
      redirect_to empresas_path
    else
      render 'new_edit'
    end
  end

  #----------------------------------------------------------------------------------------------
  def create
    begin
      empresa = Usuario.where("id = #{current_usuario.id}").first.consultor.empresa
      @registro = Expediente.new(lista_params)
      @registro.empresa_id = empresa.id

      # busqueda y creacion del nuevo codigo a partir del ultimo creado
      ultimoExpediente = Expediente.last
      autonumerico = ultimoExpediente.nil? ? 0 : ultimoExpediente.codigo[3..7].to_i
      autonumerico += 1 
      anioActual = Date.today.strftime("%y")
      autonumericoTxt = anioActual.to_s + '-' + autonumerico.to_s.rjust(4, '0')
      @registro.codigo = autonumericoTxt

      if @registro.valid?
        unless @registro.save
          raise 'No se pudo guardar'
        end
      else
        raise 'Campos con errores'
      end

    rescue => e
      @titulo   = "Expedientes"
      @subtitulo = "Crear expediente de la empresa '#{empresa.nombre}'"
      flash[:error] = 'Error al intentar registrar los cambios en la Base de Datos.'
      render 'new_edit'
    else
      flash[:success] = 'Nuevo registro creado correctamente.'
      redirect_to @registro
    end
  end

  #---------------------------
  def edit
    begin
      @registro = Expediente.find(params[:id])
      @titulo   = "Editar expediente " + @registro.codigo
      @subtitulo = "Empresa '#{@registro.empresa.nombre}'"
      
    rescue => e
      flash[:error] = 'Error al intentar mostrar la pantalla.'
      redirect_to empresas_path
    else
      render 'new_edit'
    end
  end

  #-----------------------------------------------------------------
  def update
    begin
      @registro = Expediente.find(params[:id])
      @registro.update(lista_params)

    rescue => e
      empresa = Usuario.where("id = #{current_usuario.id}").first.consultor.empresa
      @titulo   = "Expedientes"
      @subtitulo = "Editar expediente de la empresa '#{empresa.nombre}'"
      flash[:error] = 'Error al intentar registrar los cambios en la Base de Datos.'
      render 'new_edit'

    else
      flash[:success] = 'Los cambios se registraron correctamente.'
      redirect_to expediente_path(@registro.id)
    end
  end


 #------------------------------------------------------------------
 def destroy
  begin
    registro = Expediente.find(params[:id])
    registro.destroy
  rescue => e
    flash[:error] = 'Error al intentar borrar el registro.'
    redirect_to action: :show, id: param_id
  else
    flash[:success] = 'El registro se eliminó correctamente.'
    redirect_to action: :index
  end
end

#------------------------------------------------------------------
#------------------------------------------------------------------
private

  #------------------------------------------------------------------
  def lista_params
    params.require(:expediente).permit(:referencia, :descripcion, :archivo)
  end

end
