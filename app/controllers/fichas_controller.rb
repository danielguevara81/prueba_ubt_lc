class FichasController < ApplicationController
  #----------------------------------------------------------------------------------------------
  def create
    begin
      @registroFicha = Ficha.new(lista_params)
      @registro = Expediente.find(@registroFicha.expediente_id)
      @titulo   = "Detalle de expediente " + @registro.codigo
      @subtitulo = "Empresa '#{@registro.empresa.nombre}'"
      
      if @registroFicha.valid?
        unless @registroFicha.save
          raise 'No se pudo guardar'
        else
          @registroFicha = Ficha.new
        end
      else
        raise 'Campos con errores'
      end
      
    rescue => e
      flash[:error] = 'Error al intentar guardar el registro.'
      render 'expedientes/show'
    else
      flash[:success] = 'Nuevo registro creado correctamente.'
      redirect_to expediente_path(@registro.id)
    end
  end

  #------------------------------------------------------------------
  #------------------------------------------------------------------
  private

  #------------------------------------------------------------------
  def lista_params
    params.require(:ficha).permit(:expediente_id, :fecha_inicio, :referencia, :descripcion, :archivo)
  end
end