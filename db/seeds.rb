# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Consultor.delete_all
Usuario.delete_all
Empresa.delete_all

2.times do |index|
  empresa = Empresa.create(
      nombre: Faker::Company.name,
      nif: Faker::Company.spanish_organisation_number,
      direccion: Faker::Address.full_address,
      telefono: Faker::PhoneNumber.phone_number ,
      correo: Faker::Internet.email
  )

  usuario = Usuario.create( 
    email:'Usuario' + index.to_s + '@dominio' + index.to_s + '.com', 
    password: index.to_s + index.to_s  + index.to_s  + index.to_s  + index.to_s  + index.to_s, 
    password_confirmation: index.to_s + index.to_s  + index.to_s  + index.to_s  + index.to_s  + index.to_s
  )

  Consultor.create(
    usuario_id: usuario.id,
    empresa_id: empresa.id,
    nombre: Faker::Name.first_name,
    apellidos: Faker::Name.last_name,
    telefono: Faker::PhoneNumber.phone_number 
  )

end

