# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_18_103026) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "consultors", force: :cascade do |t|
    t.bigint "usuario_id", null: false
    t.bigint "empresa_id", null: false
    t.string "nombre", null: false
    t.string "apellidos", null: false
    t.string "telefono"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["empresa_id"], name: "index_consultors_on_empresa_id"
    t.index ["usuario_id"], name: "index_consultors_on_usuario_id"
  end

  create_table "empresas", force: :cascade do |t|
    t.string "nombre", null: false
    t.string "nif", null: false
    t.string "direccion"
    t.string "telefono"
    t.string "correo"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "expedientes", force: :cascade do |t|
    t.bigint "empresa_id", null: false
    t.string "codigo", limit: 10, null: false
    t.string "referencia", null: false
    t.string "descripcion", limit: 500
    t.string "archivo", limit: 200
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["codigo"], name: "index_expedientes_on_codigo", unique: true
    t.index ["empresa_id"], name: "index_expedientes_on_empresa_id"
  end

  create_table "fichas", force: :cascade do |t|
    t.bigint "expediente_id", null: false
    t.date "fecha_inicio", null: false
    t.string "referencia", null: false
    t.string "descripcion", limit: 500
    t.string "archivo", limit: 200
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["expediente_id"], name: "index_fichas_on_expediente_id"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_usuarios_on_email", unique: true
    t.index ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true
  end

  add_foreign_key "consultors", "empresas", on_update: :cascade, on_delete: :cascade
  add_foreign_key "consultors", "usuarios", on_update: :cascade, on_delete: :cascade
  add_foreign_key "expedientes", "empresas", on_update: :cascade, on_delete: :cascade
  add_foreign_key "fichas", "expedientes", on_update: :cascade, on_delete: :cascade
end
