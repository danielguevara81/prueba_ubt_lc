class CreateExpedientes < ActiveRecord::Migration[6.1]
  def change
    create_table :expedientes do |t|
      t.references :empresa, null: false
      t.string :codigo, limit: 10, null: false
      t.string :referencia, null: false
      t.string :descripcion, limit: 500
      t.string :archivo, limit: 200

      t.timestamps
    end

    add_index :expedientes, :codigo, unique: true
    add_foreign_key :expedientes, :empresas, on_delete: :cascade, on_update: :cascade
  end
end
