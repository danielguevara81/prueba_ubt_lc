class CreateEmpresas < ActiveRecord::Migration[6.1]
  def change
    create_table :empresas do |t|
      t.string :nombre, null: false
      t.string :nif, null: false
      t.string :direccion
      t.string :telefono
      t.string :correo

      t.timestamps
    end
  end
end
