class CreateFichas < ActiveRecord::Migration[6.1]
  def change
    create_table :fichas do |t|
      t.references :expediente, null: false
      t.date :fecha_inicio, null: false
      t.string :referencia, null: false
      t.string :descripcion, limit: 500
      t.string :archivo, limit: 200

      t.timestamps
    end

    add_foreign_key :fichas, :expedientes, on_delete: :cascade, on_update: :cascade
  end
end
