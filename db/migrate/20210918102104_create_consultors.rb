class CreateConsultors < ActiveRecord::Migration[6.1]
  def change
    create_table :consultors do |t|
      t.references :usuario, null: false
      t.references :empresa, null: false
      t.string :nombre, null: false
      t.string :apellidos, null: false
      t.string :telefono

      t.timestamps
    end

    add_foreign_key :consultors, :usuarios, on_delete: :cascade, on_update: :cascade
    add_foreign_key :consultors, :empresas, on_delete: :cascade, on_update: :cascade
  end
end
