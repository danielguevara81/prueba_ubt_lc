require "test_helper"

class FichasControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get fichas_create_url
    assert_response :success
  end

  test "should get edit" do
    get fichas_edit_url
    assert_response :success
  end
end
