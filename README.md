# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 3.0.2

* BD PostgreSQL

* Usuarios: usuario0@dominio0.com / usuario1@dominio1.com 

* Claves: 000000 / 111111 

* Deployment instructions:
  * Descargar la aplicacion del repositorio: `git clone https://danielguevara81@bitbucket.org/danielguevara81/prueba_ubt_lc.git`.
  * Entrar en el directorio `cd prueba_ubt_lc`
  * Instalar las gemas: `bundle install`
  * Instalar las librerias js: `yarn install`
  * Crear BD '`prueba_ubt_dev`' (para entorno de desarrollo)
  * Ejecutar las migraciones: `rails db:migrate`
  * Ejecutar los seed: `rails db:seed`
  * Ejecutar el servidor: `rails s`
  * Abrir el explorador e ir a la URL: `localhost:3000`
  * Entrar a la aplicacion con las contraseñas antes indicadas.

