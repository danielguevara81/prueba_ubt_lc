Rails.application.routes.draw do
  get 'home/index'
  devise_for :usuarios
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources 'expedientes'
  resources 'fichas'

  root to: "home#index"

end