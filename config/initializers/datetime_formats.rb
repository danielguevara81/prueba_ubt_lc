#################### FORMATOS PARA FECHA ########################
Date::DATE_FORMATS[:month_and_year] = '%B %Y'
Date::DATE_FORMATS[:short_ordinal] = ->(date) { date.strftime("%B #{date.day.ordinalize}") }
Date::DATE_FORMATS[:dia_mes_ano] = '%d/%m/%Y'
Date::DATE_FORMATS[:ano_mes_dia] = '%Y/%m/%d'

#################### FORMATOS PARA HORA ########################
Time::DATE_FORMATS[:es] = '%d/%m/%Y %I:%M %p'
Time::DATE_FORMATS[:hora_am_pm] = '%I:%M %p'
